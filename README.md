# Interval Timer

An app for making the phone vibrate at regular intervals.

Intervals of different lengths can be set and it will cycle through them, looping at the end to a point of your choice.

Written in Dart/Flutter by Iain Findlay [(Fyll.nds)](https://fyll-nds/bitbucket.io/).

The audio tone is from [(http://soundbible.com/1815-A-Tone.html)](http://soundbible.com/1815-A-Tone.html).

# Download

The .apk can be downloaded [here](https://bitbucket.org/Fyll-nds/interval-timer/downloads/app-release.apk).

# Manual

* Use the + button to add a new interval. This will make a pop-up appear where you can type in the interval length (in minutes) and choose whether to vibrate or play a tone.

* Add as many intervals as you want, and the arrows on the left will show the order and how they loop.
    * You can tap a faded-out arrow to make it loop to that point instead, for example, if you want intervals that go, 1 minute, 2 minutes, 2 minutes, 2 minutes, ....
    * If you tap the loop-back arrow, it will turn the looping off, so that the intervals will terminate when they are all complete.

* Press the green play button to start the intervals running.

* The timing screen displays the total time elapsed at the top, how long is left in the current interval in the middle, and how long the next interval will be at the bottom. You can tap the screen at any time to pause the interval.

* When paused, you have the choice to continue from where you paused it, restart the current interval, skip to the next interval, or just stop and return to the other screen.

* Closing or minimising the app will stop any active intervals.

* While timing, the app keeps the screen locked into an "awake" state.

