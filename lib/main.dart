import "package:flutter/material.dart";
import "package:flutter/services.dart";

import "package:vibration/vibration.dart";
import "package:audioplayers/audio_cache.dart";
import "package:wakelock/wakelock.dart";

import "LoopPainter.dart";

//Make sure the device is portrait.
void main() { runApp(MyApp()); }

class MyApp extends StatelessWidget
{
    Widget build(BuildContext context) {
        SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

        return MaterialApp(
            title: "Title",
            theme: ThemeData(primarySwatch: Colors.grey),
            home: MainWidget()
        );
    }
}

class MainWidget extends StatefulWidget
{
    MainWidget({Key key}) : super(key: key) {}
    MainWidgetState createState() { return MainWidgetState(); }
}

class MainWidgetState extends State<MainWidget> with TickerProviderStateMixin
{
    List<int> ints_ = [];
    List<bool> vibrate_ = [];
    bool newShouldVibrate_ = true;
    int loopTo_ = 0;

    AnimationController fadeAnimCtrl_ = null;
    bool menuOpen_ = false;
    TextEditingController textCtrl_ = TextEditingController();
    String addErrMsg_ = "";

    AnimationController switchAnimCtrl_ = null;

    bool timeOpen_ = false;
    bool timePaused_ = false;

    int index_ = 0;
    int nextIndex_ = 0;
    int sec_ = 0;
    int min_ = 0;
    int mLeft_ = 0;
    int sLeft_ = 0;

    MainWidgetState () {
        fadeAnimCtrl_ = AnimationController(value: 0.0, duration: Duration(milliseconds: 256), vsync: this);
        switchAnimCtrl_ = AnimationController(value: 0.0, duration: Duration(milliseconds: 256), vsync: this);
    }

    @override void dispose() {
        if(timeOpen_)
            Wakelock.disable();
    }

    Widget build(BuildContext ctx) {
        Size dim = Size(MediaQuery.of(ctx).size.width, MediaQuery.of(ctx).size.height - MediaQuery.of(ctx).viewPadding.top - MediaQuery.of(ctx).viewPadding.bottom);

        return Scaffold(
            body: Stack(children: [
                SlideTransition(
                    position: Tween(begin: Offset(0.0, 0.0), end: Offset(0.0, -1.0)).animate(switchAnimCtrl_),
                    child: ListView(children: [
                        Container(
                            width: dim.width * 0.8, height: dim.height * 0.59,
                            child: ((ints_.length > 0) ? listBuilder(ctx) : Center(child: Text("Use the + button below to add an interval.")))
                        ),
                        Container(height: dim.height * 0.1, child: roundedButton(Icon(Icons.add, size: dim.height * 0.08, color: Color(0x44000000)), openMenu)),
                        Container(height: dim.height * 0.01),
                        Container(
                            height: dim.height * 0.3,
                            child: roundedButton(Center(child: Icon(Icons.play_arrow, size: dim.height * 0.24, color: Color(0x4400ff00))), ((ints_.length > 0) ? startTiming : null))
                        )
                    ])
                ),
                SlideTransition(
                    position: Tween(begin: Offset(0.0, 1.0), end: Offset(0.0, 0.0)).animate(switchAnimCtrl_),
                    child: ListView(children: [
                        Container(
                            width: dim.width, height: dim.height,
                            child: roundedButton(
                                Stack(children: [
                                    Center(child: Icon(Icons.pause, size: dim.width * 0.8, color: Color(0x44ffff00))),
                                    Center(child: Column(children: [
                                        Expanded(child: Container()),
                                        Text("Total Time Elapsed:", textScaleFactor: 1.6),
                                        Text(tToStr(min_, sec_), textScaleFactor: 1.6),
                                        Expanded(child: Container()),
                                        Expanded(child: Container()),
                                        Text(tToStr(mLeft_, sLeft_), textScaleFactor: 4.0),
                                        Expanded(child: Container()),
                                        Expanded(child: Container()),
                                        Text("Next Interval:", textScaleFactor: 1.6),
                                        Text(((ints_.length == 0 || nextIndex_ < 0) ? "--:--" : tToStr(ints_[nextIndex_], 0)), textScaleFactor: 1.6),
                                        Expanded(child: Container())
                                    ]))
                                ]),
                                () { fadeAnimCtrl_.forward(); setState(() { timePaused_ = true; menuOpen_ = true; }); }
                            )
                        )
                    ])
                ),
                (menuOpen_ ? FadeTransition(
                    opacity: fadeAnimCtrl_,
                    child: Stack(children: [
                        Container(width: dim.width, height: MediaQuery.of(ctx).size.height, color: Color(0x88000000)),
                        Center(child: Container(
                            width: dim.width * 3.0 / 4.0, height: dim.height * 3.0 / 4.0,
                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(20.0))),
                            padding: EdgeInsets.all(20.0),
                            child: overlayBuilder(ctx)
                        ))
                    ])
                ) : Container())
            ])
        );
    }

    Widget roundedButton(Widget child, void Function() onPress) {
        return RaisedButton(child: child, onPressed: onPress, shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))));
    }

    Widget listBuilder(BuildContext ctx) {
        Size endDim = Size(0.15, 0.15) * MediaQuery.of(ctx).size.width;
        List<Widget> tiles = [];

        for(int i = 0; i < ints_.length; ++i) {
            tiles.add(Container(height: endDim.height, child: Row(children: [
                GestureDetector(
                    //The Stack forces it to repaint the canvas when loopTo_ is changed.
                    child: Stack(children: [
                        Text(loopTo_.toString() + ints_.length.toString(), style: TextStyle(color: Color(0x00ffffff))),
                        CustomPaint(size: endDim, painter: LoopPainter((i == 0), (i == ints_.length - 1), i, loopTo_))
                    ]),
                    onTapUp: (TapUpDetails) { setState(() {
                        if(loopTo_ == i)
                            loopTo_ = -1;
                        else
                            loopTo_ = i;
                    }); }
                ),
                Expanded(child: Column(children: [
                    Expanded(child: Center(child: Text(ints_[i].toString() + " Minute" + ((ints_[i] == 1) ? "" : "s"), textAlign: TextAlign.center))),
                    Expanded(child: Center(child: Text("Then " + (vibrate_[i] ? "Vibrate" : "Beep"), textAlign: TextAlign.center)))
                ])),
                PopupMenuButton<int>(
                    child: Container(child: Icon(Icons.more_vert), width: endDim.width, height: endDim.height),
                    itemBuilder: (BuildContext) { return [
                        PopupMenuItem<int>(value: 0, child: ListTile(leading: Icon(Icons.arrow_upward), title: Text("Up"))),
                        PopupMenuItem<int>(value: 1, child: ListTile(leading: Icon(Icons.arrow_downward), title: Text("Down"))),
                        PopupMenuDivider(),
                        PopupMenuItem<int>(value: 2, child: ListTile(leading: Icon(Icons.delete), title: Text("Delete"))),
                    ]; },
                    onSelected: (int which) {
                        if(which == 0 && i > 0) {
                            setState(() {
                                int hold = ints_[i - 1];
                                ints_[i - 1] = ints_[i];
                                ints_[i] = hold;

                                bool hold2 = vibrate_[i - 1];
                                vibrate_[i - 1] = vibrate_[i];
                                vibrate_[i] = hold2;
                            });
                        }
                        else if(which == 1 && i < ints_.length - 1) {
                            setState(() {
                                int hold = ints_[i + 1];
                                ints_[i + 1] = ints_[i];
                                ints_[i] = hold;

                                bool hold2 = vibrate_[i + 1];
                                vibrate_[i + 1] = vibrate_[i];
                                vibrate_[i] = hold2;
                            });
                        }
                        else if(which == 2) {
                            setState(() {
                                if(loopTo_ > i)
                                    --loopTo_;
                                ints_.removeAt(i);
                                vibrate_.removeAt(i);
                            });
                        }
                    }
                )
            ])));
        }

        return ListView(children: tiles);
    }

    Widget overlayBuilder(BuildContext ctx) {
        if(timeOpen_) {
            return ListView(children: [
                Text("Paused", textAlign: TextAlign.center, textScaleFactor: 1.2, style: TextStyle(fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),
                Container(height: 20),
                roundedButton(Text("Continue"),
                    () { fadeAnimCtrl_.reverse().whenComplete(() { menuOpen_ = false; timePaused_ = false; updateTime(); }); }
                ),
                roundedButton(Text("Restart Interval"),
                    () { setState(() { mLeft_ = ints_[index_]; sLeft_ = 0; }); fadeAnimCtrl_.reverse().whenComplete(() { menuOpen_ = false; timePaused_ = false; updateTime(); }); }
                ),
                roundedButton(Text("Next Interval"),
                    () { setState(() { updateIndex(); }); fadeAnimCtrl_.reverse().whenComplete(() { menuOpen_ = false; timePaused_ = false; updateTime(); }); }
                ),
                roundedButton(Text("Stop"),
                    () { fadeAnimCtrl_.reverse().whenComplete(() { menuOpen_ = false; timeOpen_ = false; timePaused_ = false; switchAnimCtrl_.reverse(); Wakelock.disable(); setState(() {}); }); }
                )
            ]);
        }
        else {
            return ListView(children: [
                Text("Add Interval", textAlign: TextAlign.center, textScaleFactor: 1.2, style: TextStyle(fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),
                Container(height: 20),
                Text("Duration (minutes):"),
                TextField(
                    controller: textCtrl_,
                    keyboardType: TextInputType.number,
                    onSubmitted: (String) { closeMenuAndAdd(); }
                ),
                Container(height: 20),
                Row(children: [
                    Expanded(child: Container()),
                    Text("Vibrate: "),
                    Radio<bool>(value: true, groupValue: newShouldVibrate_, onChanged: (bool newVal) { setState(() { newShouldVibrate_ = newVal; }); }),
                    Expanded(child: Container()),
                    Text("Beep: "),
                    Radio<bool>(value: false, groupValue: newShouldVibrate_, onChanged: (bool newVal) { setState(() { newShouldVibrate_ = newVal; }); }),
                    Expanded(child: Container())
                ]),
                Container(height: 20),
                roundedButton(Text("Add"), closeMenuAndAdd),
                roundedButton(Text("Cancel"), () { fadeAnimCtrl_.reverse().whenComplete(() { setState(() { menuOpen_ = false; }); }); }),
                Text(addErrMsg_, textAlign: TextAlign.center, style: TextStyle(color: Colors.red))
            ]);
        }
    }

    void openMenu() { setState(() { textCtrl_.text = ""; menuOpen_ = true; addErrMsg_ = ""; }); fadeAnimCtrl_.forward(); }
    void closeMenuAndAdd() {
        int val = int.tryParse(textCtrl_.text);
        if(val == null)
            setState(() { addErrMsg_ = "Value must be an integer."; });
        else if(val <= 0)
            setState(() { addErrMsg_ = "Value must be positive."; });
        else {
            ints_.add(val);
            vibrate_.add(newShouldVibrate_);
            newShouldVibrate_ = true;

            fadeAnimCtrl_.reverse().whenComplete(() { setState(() { menuOpen_ = false; }); });
        }
    }

    String tToStr(int m, int s) { return m.toString().padLeft(2, "0") + ":" + s.toString().padLeft(2, "0"); }

    void updateIndex() {
        index_ = nextIndex_;
        if(index_ < ints_.length - 1)
            nextIndex_ = index_ + 1;
        else
            nextIndex_ = loopTo_;
    }

    void startTiming() {
        timeOpen_ = true;
        nextIndex_ = 0;
        updateIndex();

        sec_ = 0;
        min_ = 0;

        switchAnimCtrl_.forward();

        Future.delayed(Duration(seconds: 1), updateTime);

        mLeft_ = ints_[index_];
        sLeft_ = 0;

        Wakelock.enable();
        setState(() {});
    }

    void updateTime() {
        if(!timeOpen_ || timePaused_)
            return;

        ++sec_;
        if(sec_ >= 60) {
            sec_ -= 60;
            ++min_;
        }

        --sLeft_;
        if(sLeft_ < 0) {
            sLeft_ += 60;
            --mLeft_;
        }
        else if(sLeft_ == 0 && mLeft_ == 0) {
            if(vibrate_[index_])
                Vibration.vibrate();
            else
                AudioCache().play("tone.mp3");

            //Stop if you weren't supposed to loop.
            if(nextIndex_ < 0) {
                timeOpen_ = false;
                switchAnimCtrl_.reverse();

                Wakelock.disable();
                return;
            }

            updateIndex();

            mLeft_ = ints_[index_];
            sLeft_ = 0;
        }

        Future.delayed(Duration(seconds: 1), updateTime);

        setState(() {});
    }
}
