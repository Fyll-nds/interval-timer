import "package:flutter/material.dart";
import "dart:math" as dma;

class LoopPainter extends CustomPainter
{
    Paint circlePnt_ = Paint();
    Paint linePnt_ = Paint();
    Paint fadePnt_ = Paint();

    double circleRad_ = 8.0;
    double arrowPad_ = 16.0;
    double yCircle_ = 16.0;

    bool isStart_ = false;
    bool isEnd_ = false;

    int index_ = -1;
    int loopTo_ = -1;

    LoopPainter(bool isStart, bool isEnd, int index, int loopTo) {
        circlePnt_.color = Color(0xff4444ff);
        linePnt_.color = Color(0xff0000ff);
        linePnt_.style = PaintingStyle.stroke;
        linePnt_.strokeWidth = 4;
        fadePnt_.color = Color(0x220000ff);
        fadePnt_.strokeWidth = 4;

        isStart_ = isStart;
        isEnd_ = isEnd;
        index_ = index;
        loopTo_ = loopTo;
    }

    bool shouldRepaint(LoopPainter oldDelegate) { return (index_ != oldDelegate.index_); }
    void paint(Canvas cvs, Size size) {
        cvs.drawCircle(Offset(size.width - circleRad_, yCircle_), circleRad_, circlePnt_);

        //The first one has no line in at the top.
        if(!isStart_) {
            Offset arrowTip = Offset(size.width - circleRad_, yCircle_ - circleRad_);
            cvs.drawLine(Offset(arrowTip.dx, 0.0), arrowTip, linePnt_);
            cvs.drawLine(Offset(arrowTip.dx - 8.0, arrowTip.dy - 8.0), arrowTip, linePnt_);
            cvs.drawLine(Offset(arrowTip.dx + 8.0, arrowTip.dy - 8.0), arrowTip, linePnt_);
        }

        //The last one loops back...
        if(isEnd_ && loopTo_ != -1) {
            Offset br = Offset(size.width - circleRad_, size.height);
            cvs.drawLine(Offset(br.dx, yCircle_ + circleRad_), Offset(br.dx, br.dy - arrowPad_), linePnt_);
            cvs.drawArc(Rect.fromLTWH(br.dx - (2.0 * arrowPad_), br.dy - (2.0 * arrowPad_), 2.0 * arrowPad_, 2.0 * arrowPad_), 0.0, dma.pi / 2.0, false, linePnt_);
            cvs.drawLine(Offset(br.dx - arrowPad_, br.dy), Offset(2.0 * arrowPad_, br.dy), linePnt_);
            cvs.drawArc(Rect.fromLTWH(arrowPad_, br.dy - (2.0 * arrowPad_), 2.0 * arrowPad_, 2.0 * arrowPad_), dma.pi / 2.0, dma.pi / 2.0, false, linePnt_);

            if(index_ != loopTo_)
                cvs.drawLine(Offset(arrowPad_, br.dy - arrowPad_), Offset(arrowPad_, 0.0), linePnt_);
        }
        //...while the rest just go straight down.
        else if(!isEnd_)
            cvs.drawLine(Offset(size.width - circleRad_, yCircle_ + circleRad_), Offset(size.width - circleRad_, size.height), linePnt_);

        //If this is between the end and the loop-to point, then the arrow passes through this canvas.
        if(index_ > loopTo_ && !isEnd_ && loopTo_ >= 0)
            cvs.drawLine(Offset(arrowPad_, size.height), Offset(arrowPad_, 0.0), linePnt_);

        //Make the one that is looped into bolder than the rest.
        if(index_ == loopTo_) {
            Offset arrowTip = Offset(size.width - (2.0 * circleRad_), yCircle_);
            cvs.drawArc(Rect.fromLTWH(arrowPad_, arrowTip.dy, 2.0 * arrowPad_, 2.0 * arrowPad_), dma.pi, dma.pi / 2.0, false, linePnt_);
            cvs.drawLine(Offset(2.0 * arrowPad_, arrowTip.dy), arrowTip, linePnt_);
            cvs.drawLine(Offset(arrowTip.dx - 8.0, arrowTip.dy - 8.0), arrowTip, linePnt_);
            cvs.drawLine(Offset(arrowTip.dx - 8.0, arrowTip.dy + 8.0), arrowTip, linePnt_);

            if(isEnd_)
                cvs.drawLine(Offset(arrowPad_, size.height - arrowPad_), Offset(arrowPad_, arrowTip.dy + arrowPad_), linePnt_);
            else
                cvs.drawLine(Offset(arrowPad_, size.height), Offset(arrowPad_, arrowTip.dy + arrowPad_), linePnt_);
        }
        else {
            Offset arrowTip = Offset(size.width - (2.0 * circleRad_), yCircle_);
            cvs.drawLine(Offset(arrowPad_, arrowTip.dy), arrowTip, fadePnt_);
            cvs.drawLine(Offset(arrowTip.dx - 8.0, arrowTip.dy - 8.0), arrowTip, fadePnt_);
            cvs.drawLine(Offset(arrowTip.dx - 8.0, arrowTip.dy + 8.0), arrowTip, fadePnt_);
        }
    }
}